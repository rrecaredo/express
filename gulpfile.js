var gulp        = require('gulp');
var inject      = require('gulp-inject');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var nodemon     = require('gulp-nodemon');

gulp.task('browser-sync', ['nodemon'], function () {
    browserSync({
        proxy : "localhost:3000",
        port  : 5000,
        notify: true
    });
});

gulp.task('nodemon', function (cb) {
    var called = false;
    return nodemon({
        script: 'app.js',
        ignore: [
            'gulpfile.js',
            'node_modules/'
        ]
    }).on('start', function () {
        if (!called) {
            called = true;
            cb();
        }
    }).on('restart', function () {
        setTimeout(function () {
            reload({stream: false});
        }, 1000);
    });
});

gulp.task('inject', function () {

    var target  = gulp.src('./src/views/*.html');
    var sources = gulp.src(
        ['./node_modules/jquery/dist/jquery.min.js', './public/**/*.js', './public/**/*.css', './src/**/*.js', './src/**/*.css'],
        {read: false});

    return target.pipe(inject(sources, {ignorePath: 'public'}))
        .pipe(gulp.dest('./src/views'));
});

gulp.task('reload', reload);

gulp.task('watch', function () {
    gulp.watch(["./public/**/*.*", "./src/views/*.*"], ['reload']);
});

gulp.task('default', ['inject', 'browser-sync', 'watch']);