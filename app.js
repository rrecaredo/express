var express      = require('express');
var app          = express();
var port         = 3000;
var bookRouter   = express.Router();
var passport     = require('passport');
var cookieParser = require('cookie-parser');
var passport     = require('passport');
var session      = require('express-session');
// var sql        = require('mssql');

var model = {
    title: 'Hello from render',
    nav  : require('./src/models/navigation')
};

var config = {
    user    : 'rrecaredo',
    password: 'cidad$$',
    server  : 'LENOVO-PC\\SQLEXPRESS',
    database: 'books'
}

// sql.connect(config, function(err) {
//     if (err) {
//         console.log(err);
//     } else {
//         console.log('Connected to DB');
//     }
// });

var bookRouter  = require('./src/routes/bookRoutes')(model);
var adminRouter = require('./src/routes/adminRoutes')(model);
var authRouter  = require('./src/routes/authRoutes')(model);

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(session({secret: 'library'}));

app.set('views', './src/views');
app.set('view engine', 'ejs');

app.use('/books', bookRouter);
app.use('/admin', adminRouter);
app.use('/auth', authRouter);

app.get('/', (req, res) => {
    res.render('index', model);
});

app.listen(port, (err) => {
    if (err) {
        console.log(err);
    }
    console.log(`Listening on port ${port}`);
});