var navigation = [
    {
        Link: '/books',
        Text: 'Books'
    },
    {
        Link: '/authors',
        Text: 'Authors'
    }
];

module.exports = navigation;