var express = require('express');

var bookRouter = express.Router();
var books      = require('../models/books');
var sql        = require('mssql');

var router     = function (model) {
    bookRouter.route('/')
        .get(function (req, res) {
            var request = new sql.Request();

            request.query('select * from dbo.books', function (err, recordset) {
                res.render('bookListView', Object.assign(model, {books: recordset}));
            });
        });

    bookRouter.route('/:id')
        .all(function (req, res, next) {
            var id = req.params.id;
            var ps = new sql.PreparedStatement();
            ps.input('id', sql.Int);
            ps.prepare('select * from dbo.books where id = @id', function (err) {
                ps.execute({id: req.params.id}, function (err, recordset) {
                    if (recordset.length === 0) {
                        res.status(404).send('Not Found');
                    } else {
                        req.book = recordset[0];
                        next();
                    }
                })
            });
        })
        .get(function (req, res) {
            res.render('bookView', Object.assign(model, {book: req.book}));
        });

    return bookRouter;
}
module.exports = router;