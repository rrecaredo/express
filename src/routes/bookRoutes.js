var express    = require('express');
var bookRouter = express.Router();
var books      = require('../models/books');
// var sql        = require('mssql');
var mongodb    = require('mongodb').MongoClient;
var objectId   = require('mongodb').ObjectID;
var url        = 'mongodb://localhost:27017/libraryApp';

var router = function (model) {
    var bookController = require('../controllers/bookController')(null, model);
    bookRouter.route('/')
        .get(bookController.getIndex);

    bookRouter.route('/:id')
        .get(bookController.getById);

    return bookRouter;
}
module.exports = router;