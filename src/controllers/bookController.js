var books    = require('../models/books');
var mongodb  = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var url      = 'mongodb://localhost:27017/libraryApp';

var bookController = function (bookService, model) {
    var getIndex = function (req, res) {
        mongodb.connect(url, function (err, db) {
            var collection = db.collection('books');

            collection.find({}).toArray(
                function (err, results) {
                    res.render('bookListView', Object.assign(model, {books: results}));
                }
            );
        });
    }
    var getById  = function (req, res) {
        var id = new objectId(req.params.id);

        mongodb.connect(url, function (err, db) {
            var collection = db.collection('books');

            collection.findOne({_id: id},
                function (err, results) {
                    if (results.length === 0) {
                        res.status(404).send('Not Found');
                    } else {
                        res.render('bookView', Object.assign(model, {book: results}));
                    }
                }
            )
        });
    }

    return {
        getIndex: getIndex,
        getById : getById
    }
};

module.exports = bookController;